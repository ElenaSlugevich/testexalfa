import java.io.*;
import java.util.Arrays;


public class Alfa {

    public static void main(String[] args) throws IOException {
        File file = new File("Example.txt");
        file.createNewFile();
        FileWriter writer = new FileWriter(file);
        writer.write("15,18,1,5,7,10,20,12,4,13,9,14,2,16,8,11,17,19,3,6");
        writer.flush();
        writer.close();

        FileReader fr = new FileReader(file);

        BufferedReader bufferedReader = new BufferedReader(fr);
        String str = bufferedReader.readLine();
        bufferedReader.close();

        //Преобразуем массив символов в целую строку
        String[] strArr = str.split(",");
        //Преобразуем строку в массив целочисленных значений
        int[] arr = Arrays.stream(strArr).mapToInt(Integer::parseInt).toArray();

        System.out.println("Массив до сортировки: " + Arrays.toString(arr));
        //Сортируем массив
        Arrays.sort(arr);
        System.out.println("Сортировка массива по возрастанию: " + Arrays.toString(arr));
        //Переворачиваем сортированный массив
        for (int i = 0; i < arr.length / 2; i++) {
            int tmp = arr[i];
            arr[i] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = tmp;
        }
        System.out.println("Сортировка массива по убыванию: " + Arrays.toString(arr));
    }
}











